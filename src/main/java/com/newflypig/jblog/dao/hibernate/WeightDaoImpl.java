package com.newflypig.jblog.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.newflypig.jblog.dao.IWeightDAO;
import com.newflypig.jblog.model.Weight;

@Repository("weightDao")
public class WeightDaoImpl extends BaseHibernateDAO<Weight> implements IWeightDAO{

}
