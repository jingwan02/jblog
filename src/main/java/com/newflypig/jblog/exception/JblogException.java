package com.newflypig.jblog.exception;

public class JblogException extends RuntimeException {

	private static final long serialVersionUID = -182743095237907996L;

	public JblogException(){
		
	}
	
	public JblogException(String message) {
		super(message);
	}
}
