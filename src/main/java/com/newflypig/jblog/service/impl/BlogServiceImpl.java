package com.newflypig.jblog.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.newflypig.jblog.model.BlogCommon;
import com.newflypig.jblog.model.BlogConfig;
import com.newflypig.jblog.model.BlogSystem;
import com.newflypig.jblog.model.Menu;
import com.newflypig.jblog.service.IArchiveService;
import com.newflypig.jblog.service.IArticleService;
import com.newflypig.jblog.service.IBlogService;
import com.newflypig.jblog.service.IMenuService;
import com.newflypig.jblog.service.ISystemService;
import com.newflypig.jblog.service.ITagService;

@Service("blogService")
public class BlogServiceImpl implements IBlogService {
	
	@Autowired
	public IArticleService articleService;
	
	@Autowired
	public IMenuService menuService;
	
	@Autowired
	public ITagService tagService;
	
	@Autowired
	public ISystemService systemService;
	
	@Autowired
	public BlogCommon blogCommon;
	
	@Autowired
	public IArchiveService archiveService;
	
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<String> getBlogUrls() {
		List<String> urlsList = new ArrayList<String>();
		
		urlsList.addAll( this.articleService.findArticleUrls() );
		
		urlsList.addAll( this.menuService.findMenuUrls() );
		
		urlsList.addAll( this.tagService.findTagUrls() );
		
		return urlsList;
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public void updateStatics() {
		this.articleService.callNoResultableProc("cleanup");
		
		List<BlogSystem> systemList = this.systemService.findAll();
		List<Menu> menuList = this.menuService.findAll();
		
		BlogSystem blank = new BlogSystem();					
		
		blogCommon.setBlogTitle(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_BLOGTITLE.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		blogCommon.setBlogRecord(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_RECORD.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		blogCommon.setBlogMetas(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_METAS.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		blogCommon.setBlogJs(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_JS.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		blogCommon.setBlogDescription(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_DESC.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		blogCommon.setBlogKeywords(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_KEYWORDS.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		blogCommon.setBlogUrl(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_URL.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		blogCommon.setBlogEmail(
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_EMAIL.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue()
		);
		
		BlogConfig.DUOSHUO_ID =
				systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_DUOSHUO.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue();
		
		blogCommon.setLeftMenus(
				menuList.stream()
				.filter(menu -> menu.getIsLeft())
				.collect(Collectors.toList())
		);
		
		blogCommon.setRightMenus(
				menuList.stream()
				.filter(menu -> !menu.getIsLeft() )
				.collect(Collectors.toList())
		);
		
		BlogConfig.QN_ACCESSKEY = systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_QNACCESS.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue();
		
		BlogConfig.QN_SECRETKEY = systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_QNSECRET.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue();
		
		BlogConfig.GEETEST_AK = systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_GEETESTAK.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue();

		BlogConfig.GEETEST_SK = systemList.stream()
				.filter(blogSystem -> BlogSystem.KEY_GEETESTSK.equals(blogSystem.getKey()))
				.findFirst().orElse(blank).getValue();
		
		blogCommon.setArchives(this.archiveService.findAll());
		
		blogCommon.setWidgetList(this.articleService.findWidgets());
		
		blogCommon.setTags(this.tagService.findAll());
	}
}
