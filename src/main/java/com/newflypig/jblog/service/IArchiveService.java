package com.newflypig.jblog.service;

import com.newflypig.jblog.model.Archive;

public interface IArchiveService extends IBaseService<Archive>{
	/**
	 * 清楚所有数据，在重新整理归档数据时使用，谨慎使用
	 */
	public void clearAll();
}
