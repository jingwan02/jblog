package com.newflypig.jblog.service;

import java.io.Serializable;

import com.newflypig.jblog.io.IOperations;

public interface IBaseService<T extends Serializable> extends IOperations<T> {
}
