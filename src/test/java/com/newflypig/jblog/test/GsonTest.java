package com.newflypig.jblog.test;

import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class GsonTest {
	
	@Test
	public void test(){
		
		String json = "{\"success\": true,\"challenge_ts\": \"timestamp\",\"hostname\": \"string\",\"error-codes\": [1,2,3]}";
		
		JsonParser parser = new JsonParser();
        // The JsonElement is the root node. It can be an object, array, null or
        // java primitive.
        JsonElement element = parser.parse(json);
        // use the isxxx methods to find out the type of jsonelement. In our
        // example we know that the root object is the Albums object and
        // contains an array of dataset objects
        if (element.isJsonObject()) {
            JsonObject albums = element.getAsJsonObject();
            System.out.println(albums.get("success").getAsString());
            JsonArray datasets = albums.getAsJsonArray("error-codes");
            for (int i = 0; i < datasets.size(); i++) {
                int dataset = datasets.get(i).getAsInt();
                System.out.println(dataset);
            }
        }
	}
}
